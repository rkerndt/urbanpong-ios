//
//  InfoViewController.swift
//  UrbanPong
//
//  Created by Rickie Kerndt on 10/3/18.
//  Copyright © 2018 Rickie Kerndt. All rights reserved.
//

import Foundation
import UIKit

class InfoViewController: UIViewController {
    
    //MARK: Properties
    @IBOutlet weak var info_text_view: UITextView!
    
    //MARK: ViewController Delegates
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(true)
        print("In InfoViewController.viewWillAppear")
        let info_options = [NSAttributedString.DocumentReadingOptionKey.documentType : NSAttributedString.DocumentType.rtf]
        let info_url = Bundle.main.url(forResource: "README", withExtension: "rtf")
        let info_data = try? Data(contentsOf: info_url!)
        let info_text = try? NSAttributedString(data: info_data!, options: info_options, documentAttributes: nil)
        if info_text != nil {
            info_text_view.attributedText = info_text
        } else {
            info_text_view.attributedText = NSAttributedString(string: "Failed to parse README")
        }
    }
}
