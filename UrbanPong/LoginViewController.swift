//
//  LoginViewController.swift
//  UrbanPong
//
//  Created by Rickie Kerndt on 9/27/18.
//  Copyright © 2018 Rickie Kerndt. All rights reserved.
//

import Foundation
import UIKit

class LoginViewController: UIViewController, UITextFieldDelegate {
    
    //MARK: Properties
    @IBOutlet weak var info_button: UIButton!
    @IBOutlet weak var login_text_field: UITextField!
    
    @IBOutlet weak var login_button: UIButton!
    
    var max_login_name = 4
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.modalPresentationStyle = .fullScreen
        self.modalTransitionStyle = .coverVertical
    
    }
    
    //MARK: Viewcontroller delegates
    override func viewDidLoad() {
        super.viewDidLoad()
        self.login_text_field.delegate = self
    }
    
    //MARK: Control Actions
    
    @IBAction func show_info(_ sender: UIButton) {
    }
    
    @IBAction func unwindToLoginView(sender: UIStoryboardSegue) {
        
    }

    //MARK: TextField Delegates
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        login_button.backgroundColor = UIColor.green
        login_button.isEnabled = true
        login_text_field.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let currentCharacterCount = self.login_text_field.text?.count
        if (range.length + range.location > currentCharacterCount!) {
            return false
        }
        let new_length = currentCharacterCount! + string.count - range.length
        return new_length <= self.max_login_name
    }
    
    //MARK: Seque methods
    override func prepare(for seque: UIStoryboardSegue, sender: Any?) {
        if let destinationViewController = seque.destination as? GameViewController {
            destinationViewController.player_handle = login_text_field.text
        }
    }
}
