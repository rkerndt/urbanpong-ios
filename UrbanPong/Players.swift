//
//  Players.swift
//  UrbanPong
//
//  Created by Rickie Kerndt on 8/27/18.
//  Copyright © 2018 Rickie Kerndt. All rights reserved.
//

import Foundation
import UIKit

class Player {
    
    // Mark: Properties
    
    var name: String
    var UUID: String
    var scores: NSArray?
    
    init?(name: String, UUID: String) {
        
        if name.isEmpty || UUID.isEmpty {
            return nil
        }

        self.name = name
        self.UUID = UUID
        self.scores = nil
        
    }
    
}
