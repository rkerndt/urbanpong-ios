//
//  GameViewController.swift
//  UrbanPong
//
//  Created by Rickie Kerndt on 3/5/18.
//  Copyright © 2018 Rickie Kerndt. All rights reserved.
//

import UIKit


class GameViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    

    //MARK: Properties
    @IBOutlet weak var paddle: UIButton!
    @IBOutlet weak var control_button: UIButton!
    @IBOutlet weak var start_south: UIButton!
    @IBOutlet weak var start_north: UIButton!
    @IBOutlet weak var score_table: UITableView!
    var paddle_start: UInt64? = 0
    var paddle_elapse: UInt64? = 0
    var TimeBaseInfo = mach_timebase_info_data_t()
    var nano_conversion_factor: UInt64 = 0
    var paddle_animator: UIViewPropertyAnimator!
    var player_handle: String?
    
    // url info for game controller TODO: make this configurable in app
    var game_controller: GameControl!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

        self.modalPresentationStyle = .fullScreen
        self.modalTransitionStyle = .coverVertical
        
        // initialize machine timebase info to get values for conversion to nanoseconds
        mach_timebase_info(&self.TimeBaseInfo)
        self.nano_conversion_factor = UInt64 (TimeBaseInfo.numer) / UInt64 (TimeBaseInfo.denom)
        
        self.paddle_animator = UIViewPropertyAnimator( duration: 2.0, curve: .linear,  animations: { [weak self] in self?.paddle.alpha = 1 } )
        self.paddle_animator.pausesOnCompletion = true
        
        self.game_controller = GameControl(vc: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        if score_table != nil {
            score_table.dataSource = self
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.game_controller.initialize_game(handle: player_handle!)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //MARK: Paddle Actions
    @IBAction func paddle_down(_ sender: UIButton) {
        paddle_start = mach_absolute_time()
        paddle_animator.fractionComplete = 0.0
        paddle_animator.startAnimation()
    }
    
    @IBAction func paddle_release(_ sender: UIButton) {
        let paddle_stop = mach_absolute_time()
        paddle_animator.pauseAnimation()
        //paddle_animator.finishAnimation(at: .start)
        print(paddle_animator.state, paddle_animator.isRunning, paddle_animator.fractionComplete, "\n")
        paddle.alpha = 0.6
        paddle_elapse = (paddle_stop - paddle_start!) * nano_conversion_factor / 100000
        let elapsed_text = String(format: "%lu", paddle_elapse!)
        paddle.setTitle(elapsed_text, for: UIControl.State.normal)
        
        // send message to wack the ball
        game_controller.send_command(action: "wack", value: elapsed_text)
    }
    
    //MARK: Control Button Actions
    @IBAction func start_south_action(_ sender: UIButton) {
        game_controller.send_command(action: "start", value: "south")
    }
    
    @IBAction func start_north_action(_ sender: UIButton) {
        game_controller.send_command(action: "start", value: "north")
    }
    
    @IBAction func control_play(_ sender: UIButton) {
        game_controller.send_command(action: "play", value: "")
    }
    
    @IBAction func control_pause(_ sender: UIButton) {
        game_controller.send_command(action: "pause", value: "")
    }
    
    @IBAction func control_continue(_ sender: UIButton) {
        game_controller.send_command(action: "continue", value: "")
    }
    
    @IBAction func control_exit(_ sender: UIButton) {
        game_controller.send_command(action: "exit", value: "")
    }
    
    //MARK: control functions called from AppDelegate in response to view change and app exit
    
    func status_game() {
        game_controller.send_command(action: "status", value: "")
    }
    func pause_game() {
        game_controller.send_command(action: "pause", value: "")
    }
    
    func continue_game() {
        game_controller.send_command(action: "continue", value: "")
    }
    
    func exit_game() {
        game_controller.send_command(action: "exit", value: "")
    }
    
    //MARK: State Actions, called from game_controller to update UI according to game state. Use DispatchCentral to queue these onto the main thread since they are modifying the UI
    
    func init_state( current_state: String) {
        print("In func init_state")
        control_button.setTitle("Play", for: .normal)
        control_button.removeTarget(self, action: #selector(GameViewController.control_exit(_:)), for: .touchUpInside)
        control_button.addTarget(self, action: #selector(GameViewController.control_play(_:)), for: .touchUpInside)
        control_button.isEnabled = false
        control_button.backgroundColor = UIColor.lightGray
        start_north.isEnabled = true
        start_north.backgroundColor = UIColor.blue
        start_south.isEnabled = true
        start_south.backgroundColor = UIColor.blue
        paddle.isEnabled = false
        paddle.backgroundColor = UIColor.lightGray
    }
    
    func start_state( current_state: String) {
        print("In func start_state")
        self.control_button.setTitle("Play", for: .normal)
        self.control_button.removeTarget(self, action: nil, for: .touchUpInside)
        self.control_button.addTarget(self, action: #selector(GameViewController.control_play(_:)), for: .touchUpInside)
        self.control_button.isEnabled = true
        self.control_button.backgroundColor = UIColor.blue
        self.start_south.isEnabled = false
        self.start_south.backgroundColor = UIColor.lightGray
        self.start_north.isEnabled = false
        self.start_north.backgroundColor = UIColor.lightGray
        self.paddle.isEnabled = false
        self.paddle.backgroundColor = UIColor.lightGray
    }
    
    func play_state( current_state: String){
        print("In func play_state")
        self.control_button.setTitle("Pause", for: .normal)
        self.control_button.removeTarget(self, action: nil, for: .touchUpInside)
        self.control_button.addTarget(self, action: #selector(GameViewController.control_pause(_:)), for: .touchUpInside)
        self.paddle.isEnabled = true
        self.paddle.backgroundColor = UIColor.red
        self.control_button.isEnabled = true
        self.control_button.backgroundColor = UIColor.blue
        self.start_south.isEnabled = false
        self.start_south.backgroundColor = UIColor.lightGray
        self.start_north.isEnabled = false
        self.start_north.backgroundColor = UIColor.lightGray
    }
    
    func paused_state( current_state: String) {
        print("In func paused_state")
        self.control_button.setTitle("Continue", for: .normal)
        self.control_button.removeTarget(self, action: nil, for: .touchUpInside)
        self.control_button.addTarget(self, action: #selector(GameViewController.control_continue(_:)), for: .touchUpInside)
        self.control_button.isEnabled = true
        self.control_button.backgroundColor = UIColor.blue
        self.paddle.isEnabled = false
        self.control_button.backgroundColor = UIColor.lightGray
        self.start_south.isEnabled = false
        self.start_south.backgroundColor = UIColor.lightGray
        self.start_north.isEnabled = false
        self.start_north.backgroundColor = UIColor.lightGray
    }
        
    //MARK: TableView stubs
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var count = 0
        if game_controller.player_data != nil && game_controller.player_data.scores != nil {
            count = game_controller.player_data!.scores!.count
        }
        return count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellReuseIdentifier")
        if cell == nil {
            print("Failed to obtain new cell object")
        }
        let player_score = game_controller.player_data!.scores![indexPath.row] as! NSDictionary
        cell?.textLabel?.text = (player_score["Name"] as! String)
        let points = player_score["Score"] as! NSNumber
        cell?.detailTextLabel?.text = (points.description)
        return cell!
    }
    
    func update_scores() {
        if self.score_table != nil {
            self.score_table.reloadData()
        }
    }
    
    func urban_pong_error(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Default action"), style: .default, handler: { _ in NSLog(message)}))
        self.present(alert, animated: true, completion: nil)
    }
    
}

