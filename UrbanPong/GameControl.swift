//
//  GameControl.swift
//  UrbanPong
//
//  Created by Rickie Kerndt on 5/17/18.
//  Copyright © 2018 Rickie Kerndt. All rights reserved.
//

import Foundation
import UIKit

class GameControl {
    
    var urban_pong_url: URL!
    var urban_pong_port: Int!
    var urban_pong_protocol: String?
    var update_timer: Timer?
    var session: URLSession!
    var state: String?
    var player_data: Player!
    var vc: GameViewController
    var host = "192.168.0.100"
    var port = "8080"
    var proto = "http"
    
    init(vc: GameViewController) {
        self.urban_pong_url = URL(string: "http://" + host + ":" + port)
        self.update_timer = nil
        self.vc = vc
        state = "INIT"
    }

    func initialize_game(handle: String) {
        if self.player_data == nil {
            let uuid = UIDevice.current.identifierForVendor?.uuidString
            self.player_data = Player(name: handle, UUID: uuid!)
            self.player_data!.scores = [ ["Name": handle, "Score": 0] ]
            
            // setup http session
            let configuration = URLSessionConfiguration.ephemeral
            configuration.allowsCellularAccess = true
            configuration.timeoutIntervalForRequest = 2
            configuration.waitsForConnectivity = false
            self.session = URLSession(configuration: configuration)
            
            // send a status request to see where the game stands and
            // if someone is already playing
            self.send_command(action: "status", value: "")
        }
    }
    
    func send_command(action: String, value: String) {
        
        let json_string = String(format: "{\"Name\": \"%@\", \"UUID\": \"%@\", \"Action\": \"%@\", \"Value\": \"%@\"}", self.player_data.name, self.player_data.UUID, action, value)
        print("Sending command", json_string)
        let json_utf8 = json_string.data(using: String.Encoding.utf8)
        var request = URLRequest(url: urban_pong_url)
        request.httpMethod = "POST"
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.setValue(String(describing: json_utf8?.count), forHTTPHeaderField: "Content-Length")
        request.httpBody = json_utf8
        
        let task = session.dataTask(with: request, completionHandler: update)
        task.resume()
    }

    func dump_secIdentify(sec: SecIdentity) {
        var private_key: SecKey?
        var cert: SecCertificate?
        var status = SecIdentityCopyPrivateKey(sec, &private_key)
        var common_name: CFString?
        if status == errSecSuccess {
            CFShow(private_key)
        }
        status = SecIdentityCopyCertificate(sec, &cert)
        if status == errSecSuccess {
            let status = SecCertificateCopyCommonName(cert!, &common_name)
            if status == errSecSuccess {
                CFShow(common_name)
            }
        }
    }
    
    func dump_certificates(certs: Array<SecCertificate>) {
        for cert in certs {
            let summary = SecCertificateCopySubjectSummary(cert)
            if summary != nil {
                CFShow(summary)
            }
        }
    }

    @objc func status_game(_ timer: Timer) {
        print("Timer is requesting game status")
        send_command(action: "status", value: "")
    }

    func init_player_scores(handle: String?) {
        // creates initial player score data for a single player with name handle
        // and a score of zero
        player_data!.scores = [ ["Name": handle!, "Score": 0] ]
    }
    
    func update(data: Data?, response: URLResponse?, error: Error?) {
        // processes Status response from UrbanPong controller.
        // The resonse is a JSON object containing the following format:
        // 'Result' key has a value of either 'Status' which provides the game status or 'error'
        // 'Status' key has possible values of 'INIT', 'START', 'PLAY', 'PAUSED' which corresponds to
        // possible game states. Note: more game states may be added as future games are deployed.
        // 'Scores' key has value that is a list of dictionaries with a dictionary for each player.
        // The player's score dictionary contains the following keys/value pairs:
        //      'Name' : string representing the players handle
        //      'UUID'  : string representing a unique player identifier
        //      'Score': integer representing the players score
        if error == nil {
            print("no error")
            print(data!)
            do {
                let json =  try JSONSerialization.jsonObject(with: data!, options: []) as? [String: Any]
                let result = json!["Result"] as? String
                if result == "Status" {
                    print(result!)
                    let state = json!["State"] as? String
                    print(state!)
                    self.player_data.scores = json!["Scores"] as? NSArray
                    print(self.player_data.scores!)
                    if state == "START" && self.state != "START" {
                        print("Changing state to START")
                        DispatchQueue.main.async {
                            self.vc.start_state(current_state: self.state!)
                        }
                        self.state = "START"
                        if self.update_timer != nil {
                            DispatchQueue.main.async {
                                self.update_timer?.invalidate()
                                self.update_timer = nil
                                print("Invalidated update_timer")
                            }
                        }
                    }
                    else if state == "PLAY" && self.state != "PLAY" {
                        print("Changing state to PLAY")
                        DispatchQueue.main.async {
                            self.vc.play_state(current_state: self.state!)
                        }
                        self.state = "PLAY"
                        if self.update_timer == nil {
                            DispatchQueue.main.async {
                                self.update_timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.status_game(_:)), userInfo: nil, repeats: true)
                                if self.update_timer!.isValid {
                                    print("Started timer with update interval: ", self.update_timer!.timeInterval)
                                }
                                else {
                                    print("Failed to create a valid update timer")
                                }
                            }
                        }
                        else {
                            print("Update_timer already started")
                        }
                    }
                    else if state == "PAUSED" && self.state != "PAUSED" {
                        print("Changing state to PAUSED")
                        DispatchQueue.main.async {
                            self.vc.paused_state(current_state: self.state!)
                        }
                        self.state = "PAUSED"
                    }
                    else if state == "INIT" && self.state != "INIT" {
                        // INIT is reached if urban_pong times out the player
                        print("Changing state to INIT")
                        DispatchQueue.main.async {
                            self.vc.init_state(current_state: self.state!)
                        }
                        self.state = "INIT"
                        if self.update_timer != nil {
                            DispatchQueue.main.async {
                                self.update_timer?.invalidate()
                            }
                        }
                    }
                    DispatchQueue.main.async {
                        self.vc.update_scores()
                    }
                }
                else if result == "Error" {
                    if json!["Value"] != nil {
                        let message = json!["Value"] as! String
                        let title = "Urban Pong Error"
                        DispatchQueue.main.async {
                            self.vc.urban_pong_error(title: title, message: message)
                        }
                    }
                }
                else {
                    print("Expecting only a 'Status' response but got %s", result!)
                }
            } catch {
                print("Unexpected error creating json object from POST respose")
            }
        }
        else {
            let message = error?.localizedDescription
            let title = "Urban Pong Connection Error"
            DispatchQueue.main.async {
                self.vc.urban_pong_error(title: title, message: message!)
            }
        }
    }
    //MARK: URLSession delegate methods
    func urlSession(_ session: URLSession, didBecomeInvalidWithError error: Error?) {
        print("Urban Pong session was invalidated")
    }
    
}


